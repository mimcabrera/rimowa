var gulp 			= require('gulp');
var sass 			= require('gulp-sass');
var cssnano 		= require('gulp-cssnano');
var uglify 			= require('gulp-uglify');
var rename 			= require('gulp-rename');
var concat 			= require('gulp-concat');
var sourcemaps 		= require('gulp-sourcemaps');
var notify 			= require('gulp-notify');
var autoprefixer 	= require('gulp-autoprefixer');
var browserSync 	= require('browser-sync').create();

gulp.task('serve', ['stylevendor','scriptvendor','sass','script','html'], function () {
    browserSync.init({
        server: "./"
    });
});

// Style Vendor
gulp.task('stylevendor', function () {
	gulp.src('./assets/components/sass/**/*.css')
		.pipe(sourcemaps.init())
		.pipe(cssnano())
		.pipe(sourcemaps.write('./'))

	.pipe(gulp.dest('./assets/css/'))
	.pipe(browserSync.stream());
});

// Script Vendor
gulp.task('scriptvendor', function() {
  gulp.src('./assets/components/js/vendor/*.js')
  	.pipe(concat('plugins.js'))
	.pipe(rename({ suffix: '.min' }))	
	.pipe(uglify())
	.pipe(gulp.dest('./assets/js/'))
	.pipe(browserSync.stream());
});

// Styles
gulp.task('sass', function () {
	gulp.src('./assets/components/sass/**/*.scss')
		.pipe(sourcemaps.init())
		.pipe(sass().on('error', sass.logError))
		.pipe(autoprefixer({
			browsers: ['last 2 versions'],
			cascade: false
		}))
		.pipe(cssnano())
		.pipe(sourcemaps.write('./'))

	.pipe(gulp.dest('./assets/css/'))
	.pipe(browserSync.stream())
	.pipe(notify({ message: 'Styles task complete' }));
});

// Script
gulp.task('script', function() {
  gulp.src('./assets/components/js/*.js')
  	.pipe(concat('app.js'))
	.pipe(rename({ suffix: '.min' }))	
	.pipe(uglify())
	.pipe(gulp.dest('./assets/js/'))
	.pipe(browserSync.stream())
	.pipe(notify({ message: 'Script task complete' }));
});

// HTML
gulp.task('html', function () {
	gulp.src('./*.html')
	.pipe(browserSync.stream())
	.pipe(notify({ message: 'HTML task complete' }));
});

// Watch
gulp.task('watch', function() {
	  gulp.watch(['./assets/components/js/vendor/*.css'], ['scriptvendor']);
	  gulp.watch(['./assets/components/**/*.css'], ['stylevendor']);
	  gulp.watch(['./assets/components/**/*.scss'], ['sass']);
	  gulp.watch(['./assets/components/js/*.js'], ['script']);
	  gulp.watch(['./*.html'], ['html']);
});
 
// Default task
gulp.task('default', ['serve','watch']);