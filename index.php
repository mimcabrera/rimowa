<?php
/**
 * The template for displaying page.
 *
 * @package WordPress
 * @subpackage Rimowa
 * @since Rimowa 1.0
 */
get_header(); ?>

<article>
	<section class="title text-center" id="the-story">
		<h1 class="no-m"><span class="rimowa">RIMOWA</span><br>X<br>ONG SHUNMUGAM</h1>
	</section>
	<section class="banner vertical-center-wrapper">
		<div class="vertical-center">
			<div class="container">
				<div class="col-xs-12 text-center">
					<div class="embed-responsive embed-responsive-16by9">
						<iframe class="embed-responsive-item m-t-30" src="https://www.youtube.com/embed/yEPTLXiOFno" frameborder="0" allowfullscreen></iframe>
					</div>
					<h2>Women on a Mission</h2>
					<p class="m-t-30">Coveted for decades, RIMOWA the original luggage with iconic grooves, is celebrating exquisite workmanship and style with the debut of a new 6-piece exclusive suitcase collection, an inaugural collaboration with a Singapore fashion label. This partnership with Ong Shunmugam, pays tribute to the adventurous spirit of Asian women, and hopes to inspire and encourage even more women to travel in style and embark on their own journey of exploration and self-discovery. The exclusive collection melds together the best in both quality and style for fashionable travelers who believe that form and beauty need not be exclusive from one another.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="collection" id="the-collection">
		<div class="text-center">
			<h3 class="thin m-b-20 hidden-sm hidden-md hidden-lg">The Collection</h3>
		</div>
		<div class="container" id="the-collection-content">
			<!-- Collections -->
			<div>
				<div class="col-xs-12 col-sm-6 text-center">
					<p class="hidden-sm hidden-md hidden-lg">This collection unveils 6 key Asian travel destinations: China, Japan, India, Indonesia, Malaysia and Mongolia; each design pays tribute to the adventurous spirit of Asian women. The collaboration offers fashionable travellers with style that does not compromise quality for aesthetics.</p>
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-all.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-20">
					<h3 class="thin hidden-xs">The Collection</h3>
					<p class="hidden-xs">This collection unveils 6 key Asian travel destinations: China, Japan, India, Indonesia, Malaysia and Mongolia; each design pays tribute to the adventurous spirit of Asian women. The collaboration offers fashionable travellers with style that does not compromise quality for aesthetics.</p>
					<h3 class="text-center m-b-20">Details in the Design</h3>
					<p>The exterior of all 6 Classic Flights Multiwheel® 53 cases are coated with traditional textiles from each corresponding country by Ong Shunmugam, complemented by Singapore-based illustrator Rizibë, through illustrations on the interior featuring a charming female character on various traditional modes of transport. Available in only 3 pieces for each design, Every piece of craftwork carries its own distinctive characteristic, yet blends perfectly together as one collection.</p>

					<p>Only 3 pieces are available per luggage design, available exclusively at RIMOWA Singapore stores.</p>
				</div>
			</div>
			<!-- India -->
			<div>
				<div class="col-xs-12 col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-india.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-100">
					<div class="hidden-xs"><h3 class="thin">The Collection</h3></div>
					<h2 class="blue">India</h2>
					<p>The shimmery threads are woven into vibrant, multi-coloured bloom motifs in the fabric used for the
					India case, representing India’s grand culture such as the country’s majestic palaces and shiny jewellery.</p>

					<p>Like a hidden surprise, the peachy interior with contrasting cyan illustration of the female character on an elephant ride adds a pop of joy and laughter to every travel adventure.</p>
				</div>
			</div>
			<!-- China -->
			<div>
				<div class="col-xs-12 col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-china.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-100">
					<div class="hidden-xs"><h3 class="thin">The Collection</h3></div>
					<h2 class="blue">China</h2>
					<p>Inspired by the traditional Chinese belief in red being a symbolic colour of happiness, the distinct red floral theme of the design for the China case represents joy and good fortune.</p>

					<p>The interior motif depicts a female character admiring a Chinese temple while riding in a rickshaw – a two-wheeled passenger cart that has remained a traditional mode of transport in China.</p>
				</div>
			</div>
			<!-- Indonesia -->
			<div>
				<div class="col-xs-12 col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-indonesia.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-100">
					<div class="hidden-xs"><h3 class="thin">The Collection</h3></div>
					<h2 class="blue">Indonesia</h2>
					<p>Just like the official colours reflected on the flag of Indonesia, the design of this case is distinctively red and white, reflecting the values of courage and sacrifice as well as purity and peace.</p>
					<p>The illustration on the inner lining embodies the true bliss of summer travel, channelling elements of relaxation by the beach and under the iridescent rays of the sun, which many Indonesian islands are renowned for.</p>
				</div>
			</div>
			<!-- Japan -->
			<div>
				<div class="col-xs-12 col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-japan.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-100">
					<div class="hidden-xs"><h3 class="thin">The Collection</h3></div>
					<h2 class="blue">Japan</h2>
					<p>A symbol of the nation’s predominant belief, the Lotus flower carries grand significance in the Japanese culture. Revered for its ability to remain beautiful despite the murky conditions of its growth, the Lotus flower is a beacon of hope for all seeking to attain enlightenment.</p>
					<p>The imperial gold accents in the sea of the brown exterior inspires the bloom of a new awakening, whilst basking in the beauty of the pearly white lotus flower. Inside, the female character takes a relaxing bath in an onsen (hot spring), an activity that has remained an essential part of Japanese culture for centuries.</p>
				</div>
			</div>
			<!-- Malaysia -->
			<div>
				<div class="col-xs-12 col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-malaysia.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-100">
					<div class="hidden-xs"><h3 class="thin">The Collection</h3></div>
					<h2 class="blue">Malaysia</h2>
					<p>Being the national flower of Malaysia, the hibiscus perfectly represents the country in this collection, with its motifs elegantly paired with gold accents on the navy fabric</p>
					<p>Staying true to the roots of Malaysia, the interior lining depicts a female character rowing leisurely on a sampan – a traditional mode of transport that is unique to Malaysia and Southeast Asia.</p>
				</div>
			</div>
			<!-- Mongolia -->
			<div>
				<div class="col-xs-12 col-sm-6">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/luggage-mongolia.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 m-t-100">
					<div class="hidden-xs"><h3 class="thin">The Collection</h3></div>
					<h2 class="blue">Mongolia</h2>
					<p>Mongolia remains home to one nomadic cultures – the ethnic prints and embroidered sailboat motifs reflect the nomadic and free nature of the Mongols.</p>
					<p>On the other hand, the contrasting motorbike motifs on the interior represent the country’s gradual shift to modernization while maintaining its free spirit.</p>
				</div>
			</div>
		</div>
	</section>
	<section class="instagram-gallery" id="instagram-gallery">
		<div class="text-center">
			<h2>#RIMOWAXOS</h2>
			<h3>Receive a limited edition RIMOWA pouch</h3>
			<p>*Terms and conditions apply.</p>
		</div>
		<div class="container m-t-50" id="instagram-gallery-content">
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-01.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-02.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-03.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-04.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-01.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-02.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-03.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
				<div>	
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/ig-04.jpg" alt="">
					<p>Let's go! vacation with her RimowaSalsaAir ... #RIMOWAXOS</p>
				</div>
		</div>
	</section>

	<section class="mailing-list vertical-center-wrapper" id="mailing-list">
		<div class="vertical-center">
			<div class="vertical-center-wrapper">
				<div class="container">
				<div class="col-xs-12 col-sm-6 text-center vertical-center">
					<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/pouches.png" alt="">
				</div>
				<div class="col-xs-12 col-sm-6 vertical-center">
					<div class="form">
						<?php echo do_shortcode('[contact-form-7 id="4" title="Mailing List"]') ?>
					</div>
				</div>
				</div>
			</div>
		</div>
	</section>
	<section class="logo text-center">
		<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/rimowa-logo.jpg" alt="">
		<p>RIMOWA Singapore  Stores: <br> Mandarin Gallery #01-11 Tel: 6735 2051 <br> Suntec City (North Wing) #01-476 Tel: 63389252 <br> The Shoppes at Marina Bay Sands, Casino Level B2M-236 Tel: 6688 7104</p>
	</section>
</article>

<?php get_footer(); ?>