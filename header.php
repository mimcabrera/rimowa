<?php
/**
 * The template for displaying the header
 *
 * Displays all of the head element and everything up until the "site-content" div.
 *
 * @package WordPress
 * @subpackage Rimowa
 * @since Rimowa 1.0
 */

?>
<!DOCTYPE html>
<html lang="">
	<head>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<title>Rimowa</title>

		<!-- Bootstrap CSS -->
		<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/assets/css/app.css">

		<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>
	<body>
		<header>
			<nav>
				<ul>
					<li><a data-scroll href="#the-story">The Story</a></li>
					<li><a data-scroll href="#the-collection">The Collection</a></li>
					<li><a data-scroll href="#instagram-gallery">Instagram Gallery</a></li>
					<li><a data-scroll href="#mailing-list">Mailing List</a></li>
				</ul>
			</nav>
		</header>
<?php wp_head(); ?>
