<?php
/**
 * The template for displaying the footer
 *
 *
 *
 * @package WordPress
 * @subpackage Rimowa
 * @since Rimowa 1.0
 */
?>
		<footer class="text-center">
			<p><strong>RIMOWA</strong> X ONG SHUNMUGAM. All Rights Reserved 2016</p>
		</footer>

		<!-- jQuery -->
		<script src="//code.jquery.com/jquery.js"></script>
		<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
 		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/plugins.min.js"></script>
 		<script src="<?php echo get_stylesheet_directory_uri(); ?>/assets/js/app.min.js"></script>
	<?php wp_footer(); ?>
	</body>
</html>
